#!/bin/bash


RED='\033[0;31m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:./peLibraryExample/include
export PATH=$PATH:./peLibraryExample/src
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./peLibraryExample/lib


echo ""
echo "${BLUE}Making the .o files of the library peLibraryExample${NC}"
echo ""

cd peLibraryExample

make

cd ../


echo ""
echo "${BLUE}Making the example file with the linking of new library${NC}"
echo ""

make ExampleFile

echo ""
echo "${BLUE}Running the code...${NC}"
echo ""

./ExampleFile

echo ""
echo "${BLUE}removing everything${NC}"
echo ""

make clean 

cd peLibraryExample

make clean

cd ../
