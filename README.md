________________________________________________
<div align="center">
myLibrary Example

This is a compact example of how to structure a C++ library

Creation date: 07 May 2024          
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>


________________________________________________

# Introduction
Check all the folders and see the minimal structure you need to create your pre-compiled C++ library.

# Installation 
In order to install your library, go to

>> ./peLibraryExample

and simply type 

>> make

It will compile your library.

# Compiling your code with the new pre-compiled library
For you to be able to link the pre-compiled library to your code, you first need to correctly 
point to the directory your pre-compiled library is. 

To do that, you may compile it on standard
Linux folders, or point them to the environmental variables. You can do it by modifying your .bashrc 
or similar files or doing as in the example I give. See the CompileAndRun.sh for a complete example.

