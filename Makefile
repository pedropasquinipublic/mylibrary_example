SHELL = /bin/sh
NAME = all
MAKEFILE = Makefile

## Get all the needed objects to be linked
LIB_DIR := ./peLibraryExample/lib
LIB_NAMES := $(wildcard $(LIB_DIR)/*.o)
OBJ_LIB := $(LIB_NAMES:$(LIB_DIR)/%.o=$(LIB_DIR)/%.o)

BIN_DIR := ./
CODES := $(wildcard $(BIN_DIR)/*.cpp)
BIN := $(CODES:$(BIN_DIR)/%.cpp=%)

$(BIN): %: $(OBJ)
	$(CXX) $(CXXFLAGS) -c $@.cpp
	$(CXX) $(LDFLAGS) $@.o $(OBJ) $(OBJ_LIB) -o $(BIN_DIR)$@

.PHONY: clean, all

all: $(BIN)

clean:
	rm -f *.o $(BIN)
